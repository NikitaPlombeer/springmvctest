<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Index</title>
</head>
<body>
<script src="/js/script.js"/>

<table border="1">
    <tr>
        <th>Имя автора</th>
        <th>Количество книг</th>
    </tr>
    <%--@elvariable id="authors" type="java.util.List<com.alex.entity.Author>"--%>
    <c:forEach var="author" items="${authors}">
        <tr>
            <td align="center">
                <a href="/author/${author.id}"><c:out value="${author.name}"/></a>
            </td>
            <td align="center">
                <c:out value="${author.books.size()}"/>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
