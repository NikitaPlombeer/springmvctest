package com.alex.controller;

import com.alex.entity.Author;
import com.alex.entity.Book;
import com.alex.exception.BookAlreadyExistException;
import com.alex.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/author")
public class AuthorController {

    private AuthorService authorService;

    @RequestMapping("/{id}")
    public ModelAndView getAuthor(@PathVariable("id") long id,
                                  @RequestParam(name = "order", required = false) String order,
                                  @RequestParam(name = "error_code", required = false) Integer code) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("author");
        Author author;
        if(order == null) {
            author = authorService.getAuthor(id);
        } else {
            author = authorService.getAuthorWithOrderBy(id, order);
        }
        mv.addObject("author", author);

        if(code != null) {
            if(code == 0) {
                mv.addObject("error", "ISBN уже существует");
            }
        }
        return mv;
    }

    @RequestMapping(value = "/{id}/book", method = RequestMethod.POST)
    public String addBook(@PathVariable("id") long authorId, @ModelAttribute Book book, HttpSession httpSession) {
        try {
            authorService.addBook(authorId, book);
            return "redirect:/author/"+authorId;
        } catch (BookAlreadyExistException e) {
            e.printStackTrace();
            return "redirect:/author/"+authorId+"?error_code=0";
        }
    }

    @Autowired
    public void setAuthorService(AuthorService authorService) {
        this.authorService = authorService;
    }
}
