package com.alex.controller;

import com.alex.dto.ErrorMessage;
import com.alex.entity.Author;
import com.alex.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthorRestController {

    private AuthorService authorService;


    @RequestMapping(value = "/author/{id}", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
    @ResponseBody
    public ResponseEntity<?> getAuthor(@PathVariable("id") long id) {
        Author author = authorService.getAuthor(id);
        if(author == null) {
            return new ResponseEntity<>(new ErrorMessage("Автор с id "+id + " не найден"), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(author, HttpStatus.OK);
    }

    @Autowired
    public void setAuthorService(AuthorService authorService) {
        this.authorService = authorService;
    }
}
