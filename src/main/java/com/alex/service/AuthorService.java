package com.alex.service;

import com.alex.entity.Author;
import com.alex.entity.Book;
import com.alex.exception.BookAlreadyExistException;

import java.util.List;

public interface AuthorService {
    Author getAuthor(long id);

    Author getAuthorWithOrderBy(long id, String order);

    void addBook(long authorId, Book book) throws BookAlreadyExistException;

    List<Author> list();
}
