package com.alex.service.impl;

import com.alex.entity.Author;
import com.alex.entity.Book;
import com.alex.exception.BookAlreadyExistException;
import com.alex.repository.AuthorRepository;
import com.alex.repository.BookRepository;
import com.alex.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AuthorServiceImpl implements AuthorService {

    private AuthorRepository repository;
    private BookRepository bookRepository;
    @Override
    public Author getAuthor(long id) {
        return repository.findOne(id);
    }

    @Override
    public Author getAuthorWithOrderBy(long id, String order) {
        Author author = getAuthor(id);
        author.getBooks().sort((b1, b2) -> {
            if(order.equals("isbn"))
                return b1.getId().compareTo(b2.getId());
            if(order.equals("title"))
                return b1.getName().compareTo(b2.getName());
            if(order.equals("version"))
                return b1.getVersion() - b2.getVersion();
            return 0;
        });
        return author;
    }

    @Override
    @Transactional
    public void addBook(long authorId, Book book) throws BookAlreadyExistException {
        Book one = bookRepository.findOne(book.getId());
        if(one != null) {
            throw new BookAlreadyExistException();
        }
        book.setAuthor(getAuthor(authorId));
        bookRepository.save(book);
    }

    @Override
    public List<Author> list() {
        return repository.findAll();
    }


    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Autowired
    public void setRepository(AuthorRepository repository) {
        this.repository = repository;
    }
}
