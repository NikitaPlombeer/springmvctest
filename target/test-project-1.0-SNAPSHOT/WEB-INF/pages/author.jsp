<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Автор</title>
</head>
<body>

<%--@elvariable id="error" type="java.lang.String"--%>
<c:choose>
    <c:when test = "${error != null}">
        <p style="color:red;"><c:out value="${error}"/></p>
    </c:when>
</c:choose>

<h1>Add book</h1>
<form action="/author/${author.id}/book" method="post">
    <p>ISBN: <input name="id"/></p>
    <p>Name: <input name="name"/></p>
    <p>Version: <input name="version"/></p>
    <p><input type="submit" value="Submit"/> <input type="reset" value="Reset"/></p>
</form>
<%--@elvariable id="author" type="com.alex.entity.Author"--%>
<h2>Список книг автора: <c:out value="${author.name}"/></h2>

<table border="1">
    <tr>
        <th><a href="/author/${author.id}?order=isbn">ISBN</a></th>
        <th><a href="/author/${author.id}?order=title">Title</a></th>
        <th><a href="/author/${author.id}?order=version">Version</a></th>
    </tr>
    <c:forEach var="book" items="${author.books}">
        <tr>
            <td align="center">
                <c:out value="${book.id}" />
            </td>
            <td align="center">
                <c:out value="${book.name}"/>
            </td>
            <td align="center">
                <c:out value="${book.version}"/>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
